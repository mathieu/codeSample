#include "i.h"
#include <stdio.h>

static void f3(void) {
	printf("MAIN\n");
}

ADD_FUNC(f3);   

#define section_foreach_entry(section_name, type_t, elem)    \
	for (type_t *elem =                                      \
	     ({                                                \
	      extern type_t __start_##section_name;         \
	      &__start_##section_name;                      \
	      });                                               \
	     elem !=                                           \
	     ({                                                \
	      extern type_t __stop_##section_name;          \
	      &__stop_##section_name;                       \
	      });                                               \
	     ++elem)


int main(int argc, char *argv[])
{
	section_foreach_entry(my_array, func_ptr_t, entry) {
		entry->cb(); /* this will call f1, f2 and f3 */
	}

	return 0;
}
