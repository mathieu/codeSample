typedef void (*my_func_cb)(void);

typedef struct func_ptr_s {
	my_func_cb cb; /* function callback */
} func_ptr_t;

#define ADD_FUNC(func_cb)                        \
	static func_ptr_t ptr_##func_cb              \
__attribute((used, section("my_array"))) = { \
	.cb = func_cb,                           \
}
